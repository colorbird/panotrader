﻿ using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanoTestScript : MonoBehaviour {


	//gameobjects on Canvas
	GameObject searchInputField;
	GameObject screenGraph;
	GameObject gyroText;
	WWW www;
	//text input 
	string searchinputfieldText="";
	string gyroinputtext="";
	// yahoo url 1D,5D,3M,6M,  
	private const string oneDay="b?s=";
	private const string fiveDay="w?s=";
	private const string threeMonth="c/3m/";
	private const string sixMonth="c/6m/";
	private const string oneYear="c/1y/";
	private const string twoYear="c/2y/";
	private const string fiveYear="c/5y/";
	private const string Max ="c/my/";
	string urlRoot="http://ichart.finance.yahoo.com/";
	string duration=oneDay;
	

	//radio button code
	public void graphOptions(string time){
					
						if (time.Equals ("1D")) {
								duration = oneDay;
				        		 
						} else if (time.Equals ("5D")) {
								duration = fiveDay;
						} else if (time.Equals ("3M")) {
								duration = threeMonth;
						} else if (time.Equals ("6M")) {
								duration = sixMonth;
						} else if (time.Equals ("1Y")) {
								duration = oneYear;
						} else if (time.Equals ("2Y")) {
								duration = twoYear;
						} else if (time.Equals ("5Y")) {
								duration = fiveYear;
						} else if (time.Equals ("Max")) {
								duration = Max;
						}
				
		}

	//search button code
	public void searchButtonClick(string id){

		// input field
		searchInputField = GameObject.Find ("SearchInputField");
		gyroText = GameObject.Find ("GyroText");
		searchinputfieldText = searchInputField.transform.GetComponentInChildren<Text> ().text;
		//printing gyro values
		gyroinputtext = "acc : " + Input.acceleration.ToString() + " gyro :" + Input.gyro.attitude.ToString();
		gyroText.transform.GetComponentInChildren<Text> ().text= gyroinputtext ;
		if (id.Equals ("searchButton")) {
				StartCoroutine (getGraphImage (searchinputfieldText, duration));
			}

		}
	// one day
	 IEnumerator getGraphImage(string searchText,string duration) {
		//output image
		screenGraph = GameObject.Find ("ScreenGraph");

		//default is microsoft
		if(searchText.Equals("Input"))
		{
		www = new WWW(urlRoot+ duration+"MSFT");
		}
		else
		{
		 www = new WWW(urlRoot+ duration+searchText);
		}

		yield return www;

		screenGraph.transform.renderer.material.mainTexture = new Texture2D(128, 128, TextureFormat.RGB24, false);
		www.LoadImageIntoTexture (screenGraph.transform.renderer.material.mainTexture as Texture2D);
		
		}

		
}
