﻿using UnityEngine;
using System.Collections;
/**
 * Author Arjun Subramanian
 * Create the method 
 * 
 * 
 * **/

using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.IO;
/**
 * Companies:
 * ABT,ABBV,ACE,ACN,ACT,ADBE,ADT,AES,AET,AFL,A,GAS,APDARG,AKAM,AA,ALXN,ATIALLE,AGN,ADS,ALL,ALTR,MO,,AMZNAEE,AEP,AXP,AIG,AMT,,AMP,
 * ABC,AME,AMGN,APHAPC,,ADI,AONAPA,,AIVAAPL,AMAT,ADM,AIZ,T,ADSK,ADP,,AN,AZO,AVGO,,AVB,AVY,AVP,BHI,BLL,BAC,BK,BCR,BAX,BBT,BDX,BBBY,
 * BMSBRK.B,BBY,BIIB,BLK,HRB,BA,BWA,BXP,BSX,BMY,BRCM,BF.B,CHRW,CACVC,COG,CAM,CPB,COF,CAH,CFN,KMX,CCL,CAT,CBGCBS,CELG,CNP,CTL,CERN,
 * CF,SCHW,CHK,CVX,CMGCB,CI,,XECCINF,CTAS,CSCO,C,CTXS,CLX,CME,CMS,COH,KOCCE,CTSH,CL,CMCSA,CMA,CSC,CAG,COP,CNX,ED,STZ,GLW,COST,COV,
 * CCI,CSX,CMI,CVS,DHI,DHR,DRI,DVA,DE,DLPH,DAL,DNR,XRAY,DVN,DO,DTV,DFS,DISCA,DG,DLTR,*,,,DOV,DOW,DPS,DTEDD,DUK,DNB,ETFCEMN,ETN,EBAY
 * ,ECL,EIX,EW,EA,EMC,EMR,ESV,ETREOG,EQT,EFX,EQRESS,EL,EXC,EXPE,EXPDESRX,XOM,FFIV,,BFDO,FAST,FDX,FIS,FITB,FSLR,FE,FISV,FLIR,FLS,
 * FLRFMC,FTI,F,FRX,,FOSL,BEN,FCX,FTR,GME,GCI,GPS,GRMN,GD,GE,GGP,GIS,GM,GPC,GNW,GILD,GS,GT,GOOG,GOOGLGWW,HAL,HOG,HAR,HRS,HIG,HAS,
 * HCP,HCN,HP,HES,HPQ,HD,HON,,,RL,HSPST,HCBK,HUM,HBAN,ITW,IR,TEG,,INTCICE,,IBMIP,IPG,,IFF,INTUISRG,IVZ,,IRM,BL,EC,,NJ,JCI,JOY,JPM,
 * ,NPR,,KSU,,KEYGMCR,KMB,KIM,KMI,KLACKSS,KRFT,KR,LB,LLL,LHLRCX,LM,LEG,LEN,LUK,LLY,LNCLLTC,LMT,L,LO,LOW,LYB,MTB,MAC,M,MMM,MRO,MPC,
 * MAR,MMC,MAS,MA,MAT,MKC,MCD,MHFI,MCK,MJN,MWV,MDT,MRK,MET,MCHPMU,MSFT,MHK,TAP,MDLZMON,MNST,MCO,MS,MOS,MSI,MUR,MYL,NBR,NDAQNOV,NAVI
 * ,NTAP,NFLX,NWL,NFX,,NEM,NWSA,NEE,NLSN,NKE,NINE,,NBL,JWN,NSC,NTRS,NOC,NUNRG,NUE,NVDA,KORS,ORLY,OXY,OMC,OKE,ORCL,OI,PCG,,PCAR,PLL,
 * PH,PDCO,PAYX,BTU,PNR,PBCT,POM,PEP,PKIPRGO,PETM,PFE,PM,PSX,PNW,PXD,,PBI,PCL,PNC,RL,PPG,PPL,PX,PCP,PCLN,PFG,PG,PGR,PLD,PRU,PEG,PSA
 * ,PHM,PVH,QEP,PWR,QCOM,DGX,RRC,RTN,RHT,REGN,RF,RSG,RAI,RHI,ROK,COL,ROP,ROST,RDC,R,SWY,CRM,SNDK,SCG,SLB,SNI,STX,SEE,SRE,SHW,SIAL
 * ,SPGSJM,SNA,SO,LUV,SWN,SE,STJ,SWK,SPLS,SBUX,HOTSTT,SRCL,SYK,STI,SYMC,SYY,TROW,TGT,TEL,TE,THC,TDC,TSO,TXN,TXT,HSY,TRV,TMO,TIF,TWX
 * ,TWC,TJX,TMKTSS,TSCO,RIG,TRIP,FOXA,TSN,TYC,USB,UA,UNP,UNH,UPS,X,UTX,UNMURBN,VFC,VLO,VAR,VTR,VRSN,VZ,VRTX,,VIAB,V,VNO,VMC,WMT,WAG
 * ,DIS,GHC,WM,WAT,WLP,WFC,WDC,WU,WY,WHR,WFMWMB,IN,WEC,WYN,WYNN,XEL,XRX,XLNX,XL,XYL,YHOO,YUM,ZMH,ZION,ZTS,, ********/

/** Headers
 *  Name1
    Index2
 * Price3
   Volume4
 * Open5
 * YearHigh6
 * 
   YearLow7
  ChangeInPercent8
 * 
 * **/

//this case need name index price percentchange volume(01273)
public class ScrollableListIndices : MonoBehaviour {
	public TextAsset SP500 ;
	private const string BASE_URL = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20({0})&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
	public GameObject itemPrefab; //scrollindices prefab
	 int itemCount=50, columnCount = 1;
	//string shortDescription="";
	//int shortStringLength=2000;
	int totalrowsandColumns = 0;

	void Start()
	{   
		ArrayList Stocks = new ArrayList ();
		Stocks = StockFeed (SP500);

		String[] rows = new String[8];
		int  index = 0;
			foreach(ArrayList row in Stocks)
					{
			for(index=0;index<row.Count;index++)
			{
			//print(row[index]);
			}	

							
						
		

					}
	

		RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
		RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
		
		//calculate the width and height of each child item.
		float width = containerRectTransform.rect.width / columnCount;
		float ratio = width / rowRectTransform.rect.width;
		float height = rowRectTransform.rect.height * ratio;
		int rowCount = itemCount / columnCount;
		if (itemCount % rowCount > 0)
			rowCount++;

		//adjust the height of the container so that it will just barely fit all its children
		float scrollHeight = height * rowCount;
		containerRectTransform.offsetMin = new Vector2(containerRectTransform.offsetMin.x, -scrollHeight / 2);
		containerRectTransform.offsetMax = new Vector2(containerRectTransform.offsetMax.x, scrollHeight / 2);



		int j = 0;
		for (int i = 0; i < itemCount; i++)
		{GameObject newItem = Instantiate(itemPrefab) as GameObject;
			newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
			newItem.transform.parent = gameObject.transform;
			print(Stocks.Count);
			ArrayList Stock = (ArrayList)Stocks[i];
			for(index=0;index<Stock.Count;index++)
			{

				if(index+7<Stock.Count)
				{
				newItem.transform.FindChild("Name").GetComponentInChildren<Text> ().text=Stock[index].ToString();
				newItem.transform.FindChild("Index").GetComponentInChildren<Text> ().text=Stock[index+1].ToString();
				newItem.transform.FindChild("Price").GetComponentInChildren<Text> ().text=Stock[index+2].ToString();
				newItem.transform.FindChild("Percent Change").GetComponentInChildren<Text> ().text=Stock[index+7].ToString();
				newItem.transform.FindChild("Volume").GetComponentInChildren<Text> ().text=Stock[index+3].ToString();
				}


			}	
			//this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
			if (i % columnCount == 0)
				j++;

					//create a new item, name it, and set the parent
					
				
				

			//move and size the new item
			RectTransform rectTransform = newItem.GetComponent<RectTransform>();
			
			float x = -containerRectTransform.rect.width / 2 + width * (i % columnCount);
			float y = containerRectTransform.rect.height / 2 - height * j;
			rectTransform.offsetMin = new Vector2(x, y);
			
			x = rectTransform.offsetMin.x + width;
			y = rectTransform.offsetMin.y + height;
			rectTransform.offsetMax = new Vector2(x, y);
			
			//string newsTitle=(string)title[i+1];
			//string newsDescription=(string)description[i];
			//set the text to the heading and body.
			//if(newsTitle.Equals("")){
			//	newsTitle="Yahoo! Finance: YAHOO News";
			//}
			//newItem.transform.FindChild("Heading").GetComponent<Text> ().text=newsTitle;
			
			//if(shortStringLength<= title[i+1].ToString().Length){
			//	shortDescription=((string)title[i+1]).Substring(0,shortStringLength )+"...";
			//}else
			//{
			//shortDescription=(string)title[i+1];
			//}
			//if(newsDescription.Equals("")){
			//	newsDescription="Yahoo! Finance: YHOO News  Latest Financial News for Yahoo! Inc.";
			//}
			
			//newItem.transform.FindChild("Body").GetComponent<Text> ().text=newsDescription;

			//
//			int index=0;
//			String[] rowArray= new string[8];
//			foreach(ArrayList row in Stocks)
//			{ index=0;
//
//				foreach(string column in row)
//				{ //totalrowsandColumns++;
//					rowArray[index]=column;
//
//					index++;
//				}
//				newItem.transform.FindChild("Name").GetComponentInChildren<Text> ().text=rowArray[0];
//				newItem.transform.FindChild("Index").GetComponentInChildren<Text> ().text=rowArray[1];
//				newItem.transform.FindChild("Price").GetComponentInChildren<Text> ().text=rowArray[2];
//				newItem.transform.FindChild("Percent Change").GetComponentInChildren<Text> ().text=rowArray[7];
//				newItem.transform.FindChild("Volume").GetComponentInChildren<Text> ().text=rowArray[3];
//			}
			// string newsTitle =(string) 

		}
	}
	
	
//	// click button for search button
//	public void SearchButtonClick(string id)
//	{  
//		searchInputField= GameObject.Find("SearchInputField");
//		searchResult = GameObject.Find ("SearchResult");
//		
//		
//		if (id.Equals ("searchButton")) {
//			
//			//financial news
//			string displayNews="";
//			string searchString="";
//			FinancialNews fnews =  financialnewsScript.GetComponent<FinancialNews> ();
//			searchString= searchInputField.transform.GetComponentInChildren<Text> ().text;
//			
//			
//			
//			ArrayList News = fnews.Search (searchString);
//			
//			
//			ArrayList title=(ArrayList) News[0]; 
//			ArrayList description=(ArrayList)News[1];
//			
//			//make prefabs= titles
//			itemCount = title.Count-1;
//			
//			//assuming description length = title length -1
//			for(int i=1;i<title.Count;i++)
//			{
//				displayNews+= " "+ title[i] + "\n   " + description[i-1] +"\n";
//				
//			}
//			
//			//print (displayNews); 
//			
//			
//			//RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
//			//RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
//			
//			//calculate the width and height of each child item.
//			//float width = containerRectTransform.rect.width / columnCount;
//			//float ratio = width / rowRectTransform.rect.width;
//			//float height = rowRectTransform.rect.height * ratio;
//			//int rowCount = itemCount / columnCount;
//			//if (itemCount % rowCount > 0)
//			//	rowCount++;
//			
//			//adjust the height of the container so that it will just barely fit all its children
//			//float scrollHeight = height * rowCount;
//			//containerRectTransform.offsetMin = new Vector2(containerRectTransform.offsetMin.x, -scrollHeight / 2);
//			//containerRectTransform.offsetMax = new Vector2(containerRectTransform.offsetMax.x, scrollHeight / 2);
//			
//			int j = 0;
//			for (int i = 0; i < itemCount; i++)
//			{
//				//this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
//				if (i % columnCount == 0)
//					j++;
//				
//				//create a new item, name it, and set the parent
//				//GameObject newItem = Instantiate(itemPrefab) as GameObject;
//				GameObject newItem=GameObject.Find( gameObject.name + " item at (" + i + "," + j + ")");
//				//newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
//				//newItem.transform.parent = gameObject.transform;
//				
//				newItem.transform.FindChild("Heading").GetComponent<Text> ().text="";
//				newItem.transform.FindChild("Body").GetComponent<Text> ().text="";
//				
//				
//				string newsTitle=(string)title[i+1];
//				string newsDescription=(string)description[i];
//				//set the text to the heading and body.
//				if(newsTitle.Equals("")){
//					newsTitle="Yahoo! Finance: YAHOO News";
//				}
//				newItem.transform.FindChild("Heading").GetComponent<Text> ().text=newsTitle;
//				
//				//if(shortStringLength<= title[i+1].ToString().Length){
//				//	shortDescription=((string)title[i+1]).Substring(0,shortStringLength )+"...";
//				//}else
//				//{
//				//shortDescription=(string)title[i+1];
//				//}
//				if(newsDescription.Equals("")){
//					newsDescription="Yahoo! Finance: YHOO News  Latest Financial News for Yahoo! Inc.";
//				}
//				
//				newItem.transform.FindChild("Body").GetComponent<Text> ().text=newsDescription;
//				
//				//move and size the new item
//				//RectTransform rectTransform = newItem.GetComponent<RectTransform>();
//				
//				//float x = -containerRectTransform.rect.width / 2 + width * (i % columnCount);
//				//float y = containerRectTransform.rect.height / 2 - height * j;
//				//rectTransform.offsetMin = new Vector2(x, y);
//				
//				//x = rectTransform.offsetMin.x + width;
//				//y = rectTransform.offsetMin.y + height;
//				//rectTransform.offsetMax = new Vector2(x, y);
//			}
//		}
//		//	ArrayList News= Search("yhoo");	
//		
//		//change to this format
//		/*foreach(ArrayList content in News)
//			{
//				
//				foreach (string line in content)
//				{
//					//Console.WriteLine(line);
//					print (line);
//				}
//			}*/
//		//	ArrayList title=(ArrayList) News[0]; 
//		//	ArrayList description=(ArrayList)News[1];
//		
//		//assuming description length = title length -1
//		//	for(int i=1;i<title.Count;i++)
//		//{
//		//	displayNews+= " "+ title[i] + "\n   " + description[i-1] +"\n";
//		
//		//}
//		//newsText.transform.GetComponentInChildren<Text> ().text=displayNews;
//		//	print (displayNews);
//		
//		//}
//		
//		
//	}



	//read the text file for stock symbols modified
	public static ArrayList StockFeed(TextAsset SP500)
	{
		string[] Companies = SP500.text.Split(","[0]);
		foreach(string comp in Companies)
		{
			print (comp);
		}
		string symbolList="";
		string url = "";
		ArrayList stocks = new ArrayList();
		ArrayList Stock = new ArrayList();
		ArrayList Header = new ArrayList();
		string completeStockFeed = "";
		
		Header.Add("Name");
		Header.Add("Index");
		Header.Add("Price");
		Header.Add("Volume");
		Header.Add("Open");
		Header.Add("YearHigh");
		Header.Add("YearLow");
		Header.Add("ChangeInPercent");
		stocks.Add(Header);
		Boolean SecondTime = false;
		foreach (string line in Companies)
		{
			if(SecondTime)
			{
				symbolList += "%2C";
			}


			symbolList += "%22" + line.Trim() + "%22";
				SecondTime = true;
		
			//Console.WriteLine(url);
				print (url);

		}
		
		url = string.Format(BASE_URL, symbolList);
		symbolList = "";
		stocks.Add(Stock);
		stocks=(Fetch(url, Companies, stocks));
		foreach(ArrayList stock in stocks)
		{
			
			foreach (string line in stock)
			{
				completeStockFeed+= " " + line ;
					print (line);
			}
			
			
		}
		//print ("completeStockFeed :"+ completeStockFeed);
		return stocks;
	}
	
	//retrieve symbol lists in Arraylist Stock, an empty arraylist Stocks is passed and returned with values of stocks,called in StockFeed
	public static ArrayList Fetch(String url,string[] Symbol,ArrayList Stocks)
	{
		//ArrayList Stock = new ArrayList();
		try
		{
			XDocument doc = XDocument.Load(url);
			Parse(Stocks, doc, Symbol);
		}
		catch(Exception e)
		{
			print ("Not working" +url);
		}
		
		
		return Stocks;
	}
	
	//  parse stockdata in Arraylist stocks, this function is used in Fetch 
	private static ArrayList Parse(ArrayList Stocks, XDocument doc,string[] symbols)
	{
		XElement results = doc.Root.Element("results");
		
		foreach (string symbol in symbols)
		{   //Console.WriteLine(symbol);
			//print (symbol);
			ArrayList Stock = new ArrayList();
			if ((results.Elements("quote").First(w => w.Attribute("symbol").Value == symbol.Trim()) != null))
			{
				XElement q = results.Elements("quote").First(w => w.Attribute("symbol").Value == symbol.Trim());
				
				
				String Price = (q.Element("Ask").Value).ToString();
				//add everything to stock
				String AverageDailyVolume = (q.Element("AverageDailyVolume").Value).ToString();
				String Name = (q.Element("Name").Value).ToString();
				String Open = (q.Element("Open").Value).ToString();
				String Index = (q.Element("StockExchange").Value).ToString();
				String Volume = (q.Element("Volume").Value).ToString();
				String YearHigh = (q.Element("YearHigh").Value).ToString();
				String YearLow = (q.Element("YearLow").Value).ToString();
				String ChangeInPercent = (q.Element("ChangeinPercent").Value).ToString();
				Stock.Add(Name);
				Stock.Add(Index);
				Stock.Add(Price);
				Stock.Add(Volume);
				Stock.Add(Open);
				Stock.Add(YearHigh);
				Stock.Add(YearLow);
				Stock.Add(ChangeInPercent);
				//pass everything to Stocks
				Stocks.Add(Stock);
			}
			else
			{
				print (symbol);
			}
		}
		return Stocks;
	}
}
