﻿using UnityEngine;
using System.Collections;
//Mark the objects with the tag Draggable if you want to drag and drop objects accross the screen , work only if the camera axis is aligned
public class TouchDrag : MonoBehaviour {
	private float dist;
	private bool dragging = false;
	private Vector3 offset;
	private Transform toDrag;
	
	void Update() {
		Vector3 v3;
		
		if (Input.touchCount != 1) {
			dragging = false; 
			return;
		}
		
		Touch touch = Input.touches[0];
		Vector3 pos = touch.position;
		
		if(touch.phase == TouchPhase.Began) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(pos); 
			Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
			if(Physics.Raycast(ray, out hit) && (hit.collider.tag == "Draggable"))
			{
				Debug.Log ("Here");
				toDrag = hit.transform;
				dist = hit.transform.position.z - Camera.main.transform.position.z;
				v3 = new Vector3(pos.x, pos.y, dist);
				v3 = Camera.main.ScreenToWorldPoint(v3);
				offset = toDrag.position - v3;
				dragging = true;
			}
		}
		if (dragging && touch.phase == TouchPhase.Moved) {
			//replace with 
		    // Input.GetTouch(0).position.x
			v3= new Vector3(Input.GetTouch(0).position.x,Input.GetTouch(0).position.y,dist);
			v3 = Camera.main.ScreenToWorldPoint(v3);
			toDrag.position = v3 + offset;
		}
		if (dragging && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)) {
			dragging = false;
		}
	}
}
