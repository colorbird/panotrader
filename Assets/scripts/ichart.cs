﻿using UnityEngine;
using System.Collections;

public class ichart : MonoBehaviour {
	
	public GameObject iChartPanel;

	//the code for maps for Microsoft
	public string url = "http://ichart.yahoo.com/b?s=MSFT";
	//http://www.codeproject.com/Articles/37550/Stock-quote-and-chart-from-Yahoo-in-C
	// 1 day: http://ichart.finance.yahoo.com/b?s=MSFT
	// 5 days: http://ichart.finance.yahoo.com/w?s=MSFT
	// 3 months: http://chart.finance.yahoo.com/c/3m/msft
	// 6 months: http://chart.finance.yahoo.com/c/6m/msft
	// 1 year: http://chart.finance.yahoo.com/c/1y/msft
	// 2 years: http://chart.finance.yahoo.com/c/2y/msft
	// 5 years: http://chart.finance.yahoo.com/c/5y/msft
	// Max: http://chart.finance.yahoo.com/c/my/msft
	
	
	//	IEnumerator Start() {
	//		WWW www = new WWW(url);
	//		yield return www;
	//		renderer.material.mainTexture = www.texture;
	//		//make the shader transparent.unlit
	//	}
	
	
	void Start()
	{
		//days (1);
	}
	
	public void days(int d) {

		while(iChartPanel == null)
		{
			//i chart panel is changed in the code openPanelIndicator
			print ("never should be here, but if it does just wait until plane is assigned");
		}
		//d is in days
		switch (d)
		{
		case 1:
			print ("do you get here in case 1?");
			StartCoroutine (oneDay ());
			break;
		case 5:
			StartCoroutine (fiveDays ());
			break;
		case 90:
			StartCoroutine (threeMonths());
			break;
		case 180:
			StartCoroutine (sixMonths());
			break;
		case 365:
			StartCoroutine (oneYear());
			break;
		case 730:
			StartCoroutine(twoYears());
			break;
		case 1095:
			StartCoroutine (fiveYears ());
			break;

		}
		
		//make the shader transparent.unlit
	}

	
	IEnumerator oneDay() {
		//print ("do you get here------------------");
		WWW www = new WWW("http://ichart.finance.yahoo.com/b?s=MSFT");
		yield return www;
		//iChartPanel.transform.renderer.material.mainTexture = www.texture;
		//iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");

		//use these two lines to make it work on android
		//please apply to other functions i.e. fivedays threemonths etc.
		iChartPanel.transform.renderer.material.mainTexture = new Texture2D(128, 128, TextureFormat.RGB24, false);
		www.LoadImageIntoTexture (iChartPanel.transform.renderer.material.mainTexture as Texture2D);

//		thumbnail.mainTexture = new Texture2D(www.texture.width, www.texture.height, TextureFormat.DXT1, false);
//		www.LoadImageIntoTexture(thumbnail.mainTexture as Texture2D);
//		www.Dispose();
//		www = null;


		//make the shader transparent.unlit
	}
	IEnumerator fiveDays() {
		WWW www = new WWW("http://ichart.finance.yahoo.com/w?s=MSFT");
		yield return www;
		iChartPanel.transform.renderer.material.mainTexture = www.texture;
		iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");
		//make the shader transparent.unlit
	}
	IEnumerator threeMonths() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/3m/msft");
		yield return www;
		iChartPanel.transform.renderer.material.mainTexture = www.texture;
		iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");
		//make the shader transparent.unlit
	}
	IEnumerator sixMonths() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/6m/msft");
		yield return www;
		iChartPanel.transform.renderer.material.mainTexture = www.texture;
		iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");
		//make the shader transparent.unlit
	}
	IEnumerator oneYear() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/1y/msft");
		yield return www;
		iChartPanel.transform.renderer.material.mainTexture = www.texture;
		iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");
		//make the shader transparent.unlit
	}
	IEnumerator twoYears() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/2y/msft");
		yield return www;
		iChartPanel.transform.renderer.material.mainTexture = www.texture;
		iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");
		//make the shader transparent.unlit
	}
	IEnumerator fiveYears() {

		WWW www = new WWW("http://chart.finance.yahoo.com/c/5y/msft");
		yield return www;
		iChartPanel.transform.renderer.material.mainTexture = www.texture;
		iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");
		//make the shader transparent.unlit
	}
	// what is max?
	IEnumerator max() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/my/msft");
		yield return www;
		iChartPanel.transform.renderer.material.mainTexture = www.texture;
		iChartPanel.transform.renderer.material.shader = Shader.Find ("Unlit/Texture");
		//make the shader transparent.unlit
	}
	
	
	
	
}
