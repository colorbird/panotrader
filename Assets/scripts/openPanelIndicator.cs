﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class openPanelIndicator : MonoBehaviour {

	public int color;
	public GameObject script_PanoView;
	public GameObject Red, Orange, Yellow, Green, Blue, Navy;
	private GameObject currentActivePanel;
	public GameObject iChartPanel;
	public GameObject camera;
	public GameObject script_iChartPanel;
	public GameObject buttonsWithinPanel;

	public void changePanel(int color)
	{
		print ("do you get in here?");
		currentActivePanel.transform.GetComponent<RawImage>().enabled = true;
		foreach (Transform child in currentActivePanel.transform)
		{
			child.transform.GetComponent<RawImage> ().enabled = false;
		}

		switch(color)
		{
			case 1:

				//create new instance of the panel to fit iChart and attach to camera and destroy the older panel
				//this may be absoltet once a panel to fit all is created and confirmed to work on the glasses
				//you have to change the position of iChartPanel here to fix the bug
				
				//currentActivePanel = (GameObject)Instantiate(iChartPanel);
				//currentActivePanel.transform.parent = camera.transform;
				currentActivePanel = script_PanoView.GetComponent<changeFocus>().currentFocusPanel;
				//currentActivePanel.transform.localPosition = new Vector3 (0,0,10);
				//make it look at the camera straight
				//currentActivePanel.transform.localRotation = Quaternion.Euler(0,0,0);
				//Destroy(script_PanoView.GetComponent<changeFocus>().currentFocusPanel);
				script_PanoView.GetComponent<changeFocus>().currentFocusPanel = currentActivePanel;

				//Assign the panel to display iChart in the script iChart
				script_iChartPanel.GetComponent<ichart>().iChartPanel = currentActivePanel;


				Red.transform.GetComponent<RawImage>().enabled = false;
				foreach (Transform child in Red.transform)
				{
					child.GetComponent<RawImage>().enabled=true;
				}
				currentActivePanel = Red;
				buttonsWithinPanel.GetComponent<buttonsWithinPanel>().showButtons(1);
				break;	

			case 2:


				Orange.transform.GetComponent<RawImage>().enabled = false;
				foreach (Transform child in Orange.transform)
				{
					child.GetComponent<RawImage>().enabled=true;
				}
				currentActivePanel = Orange;
				break;

			case 3:
				Yellow.transform.GetComponent<RawImage>().enabled = false;
				foreach (Transform child in Yellow.transform)
				{
					child.GetComponent<RawImage>().enabled=true;
				}
				currentActivePanel = Yellow;
				break;

			case 4:
				Green.transform.GetComponent<RawImage>().enabled = false;
				foreach (Transform child in Green.transform)
				{
					child.GetComponent<RawImage>().enabled=true;
				}
				currentActivePanel = Green;
				break;

			case 5:
				Blue.transform.GetComponent<RawImage>().enabled = false;
				foreach (Transform child in Blue.transform)
				{
					child.GetComponent<RawImage>().enabled=true;
				}
				currentActivePanel = Blue;
				break;

			case 6:
				Navy.transform.GetComponent<RawImage>().enabled = false;
				foreach (Transform child in Navy.transform)
				{
					child.GetComponent<RawImage>().enabled=true;
				}
				currentActivePanel = Navy;
			break;
		}
	}

	// Use this for initialization
	void Start () 
	{
		currentActivePanel = Red;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}